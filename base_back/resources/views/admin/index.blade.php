@extends('adminlte::page')

@section('title', 'Panel administrativo | Acme')

@section('content_header')
    <h1 class="title-dash"> Bienvenido Panel administrativo</h1>
@stop

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <div class="card" style="width: 18rem; display: inline-block; float: right;">
                <img src="group.png" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Listado de usuarios</h5>
                  <p class="card-text">Permite administrar el listado de usuarios</p>
                  <a href="/users" class="btn btn-primary" id="bnt-ir">Ir</a>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card" style="width: 18rem; display: inline-block;">
                <img src="to-do-list.png" class="card-img-top" alt="...">
                <div class="card-body">
                  <h5 class="card-title">Listado de pruebas</h5>
                  <p class="card-text">Permite visualizar el listado de pruebas por usuario</p>
                  <a href="/pruebas-usuario" class="btn btn-primary" id="bnt-ir">Ir</a>
                </div>
            </div>
        </div>
    </div>
</div>

@stop

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/admin_custom.css') }}" >
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop