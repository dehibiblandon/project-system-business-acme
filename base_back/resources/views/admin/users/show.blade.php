@extends('adminlte::page')

@section('title', 'Ver datos de Usuario {{ $user->name_user }} | Acme ')
@section('content')

    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>

                        <th scope="col">Nombre</th>
                        <th scope="col">Cédula</th>
                        <th scope="col">Edad</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $user->name_user }}</td>
                        <td>{{ $user->cc_user }}</td>
                        <td>{{ $user->age_user }}</td>
                    </tr>

                </tbody>
            </table>
        </div>
        <div class="row">
            @foreach ($user->testacademy as $test)
                <div class="col-4">
                    <div class="card" style="display: inline-block;">
                        <img src="\img\test.jpg" class="card-img-top" alt="..." height="200px">
                        <div class="card-body">
                            <h5 class="card-title">Datos de la prueba</h5>
                            <p class="card-text"><b>Fecha:</b> {{$test->date_test}} </p>
                            <p class="card-text"><b>Tiempo:</b> {{$test->time_test}} </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>


    @endsection
