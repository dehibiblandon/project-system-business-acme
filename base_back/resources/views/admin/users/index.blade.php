@extends('adminlte::page')

@section('title', 'Panel administrativo | Acme ')

@section('content_header')
    <h1 class="title-dash">Administración de usuarios y pruebas</h1>
    <!--<div class="container">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/l91DvBTT4b8?rel=0&hl=es&cc_lang_pref=es&cc_load_policy=1"
             frameborder="0" allow="autoplay; encrypted-media" title="YouTube video player"
             allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
             allowfullscreen>
            </iframe>
        </div>
        -->

@stop

@section('content')
    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Cédula</th>
                        <th scope="col">Edad</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>

                @foreach ($users as $user)
                    <tbody>
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->name_user }}</td>
                            <td>{{ $user->cc_user }}</td>
                            <td>{{ $user->age_user }}</td>
                            <td>
                                <div class="card-action col s2">
                                    <a href="/dashboard/{{ $user->id }}" class="btn btn-outline-success"
                                        data-position="bottom">Ver Datos</a>
                                    <a href="/dashboard/{{ $user->id }}/edit" class="btn btn-outline-info"
                                        data-position="bottom">Editar</a>
                             
                                </div>
                            </td>
                        </tr>

                    </tbody>
                @endforeach
            </table>
        </div>
        <div class="row">
            <div class="col s12">
                <a href="/dashboard/create" class="" style="float: right;">Añadir Persona</a>
            </div>

        </div>
        <hr>
    </div>
@stop

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin_custom.css') }}">
@stop
