@extends('adminlte::page')

@section('title', 'Crear Usuario | Acme ')

@section('content_header')
    <h1 class="title-dash">Crear un usuario</h1>

@stop
@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (Session::has('mensaje'))
            <div>{{ Session::get('mensaje') }}</div>
        @endif
        <form action="/dashboard" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="container">

                <div class="form-group">
                    <label class="active" for="name_user">Nombre de la persona</label>
                    <input id="name_user" name="name_user" type="text" class="form-control item"
                        value="{{ old('name_user') }} " maxlength="15" minlength="3" required>
                    @if ($errors->has('name_user'))
                        <small class="form-text text-danger">{{ $errors->first('name_user') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label class="active" for="cc_user">Cédula</label>

                    <input id="cc_user" name="cc_user" type="text" class="form-control" required
                        value="{{ old('cc_user') }} ">
                    @if ($errors->has('cc_user'))
                        <small class="form-text text-danger">{{ $errors->first('cc_user') }}</small>
                    @endif
                </div>
                <div class="form-group">
                    <label class="active" for="age_user">Edad de la persona</label>

                    <input id="age_user" name="age_user" type="text" class="form-control" required
                        value="{{ old('age_user') }} ">
                    @if ($errors->has('age_user'))
                        <small class="form-text text-danger">{{ $errors->first('age_user') }}</small>
                    @endif
                </div>

                <div class="form-group col-12">
                    <label class="active" for="email">Email de la persona</label>

                    <input id="email" name="email" type="text" class="form-control" value="{{ old('email') }} " required>
                    @if ($errors->has('email'))
                        <small class="form-text text-danger">{{ $errors->first('email') }}</small>
                    @endif
                </div>

                <div class="form-group">
                    <label class="active" for="password">Password de la persona</label>

                    <input id="password" name="password" type="password" class="form-control"
                        value="{{ old('password') }} " required>
                    @if ($errors->has('email'))
                        <small class="form-text text-danger">{{ $errors->first('password') }}</small>
                    @endif
                </div>

                <div class="form-row">
                    <div class="form-group col-6">
                        <button type="submit" class="btn btn-primary btn-block create-account" data-position="bottom" style="float: right;">Guardar</button>
                    </div>
                    <div class="form-group col-6">
                        <a href="/categories" class="" data-position="bottom" style="float: left;">Volver</a>

                    </div>
                </div>

            </div>
        </form>

    </div>

@endsection
