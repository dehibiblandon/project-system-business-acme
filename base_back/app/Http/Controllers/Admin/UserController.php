<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\DB;

class Usercontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view ('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, SessionManager $sessionManager)
    {
        $find =$request->cc_user;
        $existencia = DB::table('users')
        ->select('cc_user')
        ->where('cc_user', '=', $find)
        ->get();
        if(count($existencia) >= 1) {
            $sessionManager->flash('mensaje', 'Ya Existen Personas Registradas con la Cédula: '.$find);
            return view('admin.users.create');
        }else{


        $objUser= new User();
        $objUser->name_user = $request->name_user;
        $objUser->cc_user = $request->cc_user;
        $objUser->age_user = $request->age_user;
        $objUser->email = $request->email;
        $objUser->password = $request->password;
        
        $objUser->save();
        return \redirect ('/dashboard')->with('mensaje', 'Datos creados');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show', [
            'user' => $user,            
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', [
            'user' => $user,            
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name_user = $request->name_user;
        $user->cc_user = $request->cc_user;
        $user->age_user = $request->age_user;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->update();
        return \redirect ('/dashboard')->with('mensaje', 'Datos actualizados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
