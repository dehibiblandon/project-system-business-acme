<?php

namespace App\Http\Controllers;

use App\Models\TestAcademy;
use Illuminate\Http\Request;

class TestAcademyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TestAcademy  $testAcademy
     * @return \Illuminate\Http\Response
     */
    public function show(TestAcademy $testAcademy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TestAcademy  $testAcademy
     * @return \Illuminate\Http\Response
     */
    public function edit(TestAcademy $testAcademy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TestAcademy  $testAcademy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestAcademy $testAcademy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TestAcademy  $testAcademy
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestAcademy $testAcademy)
    {
        //
    }
}
