<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function find($cedula){
        $existencia = DB::table('users')
        ->select('cc_user')
        ->where('cc_user', '=', $cedula)
        ->get();
        if(count($existencia) >= 1) { 
            	
            $users = User::join('test_academies', 'test_academies.user_id', '=', 'users.id')
            ->where('users.cc_user', '=', $cedula)
            ->get(['test_academies.*', 'users.name_user']);     
                 
            //return $users;
            return response()->json($users);
           /* return  DB::table('test_academies')
            ->where('user_id', '=', $cedula)
            ->get();*/
        }else{
            
            return response()->noContent();
        }
    }
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
