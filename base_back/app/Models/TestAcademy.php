<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestAcademy extends Model
{
    use HasFactory;
    protected $fillable = [
        'date_test',
        'time_test',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
