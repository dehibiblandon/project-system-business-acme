<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\Usercontroller;
use App\Http\Controllers\Admin\TestAcademyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', [HomeController::class, 'welcome']);
Route::resource('/dashboard', Usercontroller::class)->names('admin.users');
Route::resource('/dashboard/{user}/test', TestAcademyController::class)->names('admin.users');

