<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\TestAcademyController;
use App\Http\Controllers\Api\V1\UserController;
use App\Models\TestAcademy;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::post('v1/save_test', [TestAcademyController::class,'store']);
Route::get('v1/user/{user}/list_test', [UserController::class,'find']);

Route::post('v1/test', function () {

    return TestAcademy::create([
        'date_test'=>'2012/05/10',
         'time_test'=>'12:00',
         'user_id'=>1
        

    ]);

});


