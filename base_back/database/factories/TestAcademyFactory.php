<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TestAcademyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_test' => $this->faker->date(),
            'time_test' => $this->faker->time(),
            'user_id' =>$this->faker->numberBetween(1,3),
            //'cc_user_test' => '12345',
        ];
    }
}
