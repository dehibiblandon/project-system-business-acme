<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();

        $user->name_user = 'Dehibi Blandon';
        $user->cc_user='123456789';
        $user->age_user= '30';
        $user->email = 'ingenieria@gmail.com';
        $user->password = '$2y$10$NTslMzsTR5ztapbFc.s0Q.piG1A1yM8g2NBcqndYR.pEC.fViqXGy'; // 123456
        $user->remember_token = Str::random(10);
        $user->save();

        $user->assignRole('Admin');

        User::factory(10)->create();
    }
}
