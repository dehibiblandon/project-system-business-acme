<?php

namespace Database\Seeders;

use App\Models\TestAcademy;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolSeeder::class);
        $this->call(UserSeeder::class);
        TestAcademy::factory(30)->create();
    }
}
