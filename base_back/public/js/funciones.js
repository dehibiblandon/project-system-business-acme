function consultar(event) {
    event.preventDefault();
    // consultar por parametro
    var ced = document.getElementById("consulta_cedula").value;
    var request = new Request('http://127.0.0.1:8000/api/v1/user/'+ ced +'/list_test', {
        method: 'Get',
    });

    fetch(request)
    .then(function(response) {
        return response.text();
    })
    .then(function(data) {
       console.log(data);
        if(data){
            sessionStorage.setItem("informacion_pruebas_usuario", data);
            window.location = 'simulator.html';
        }else{
            alert("No hay Datos asociados");            
            sessionStorage.removeItem("informacion_pruebas_usuario");
        }
        
        //guardar los datos en sesionStorege el método recibe dos parametros nombre y valor

    })
    .catch(function(err) {
        console.error(err);
    });

   

}
