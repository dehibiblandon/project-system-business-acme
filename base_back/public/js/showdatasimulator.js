document.addEventListener("DOMContentLoaded", () => {
    

    function getDataSesionStorage(){
        var dataStorage = JSON.parse(sessionStorage.getItem("informacion_pruebas_usuario"));
        showData(dataStorage);
        //console.log(dataStorage);
     
    }
    function showData(data)
    {
        var nameUser = document.querySelector("#user_name");
        var elementTable = document.querySelector("#puntuaciones tbody");         
        var tr, td1, td2;
        
        nameUser.innerText = data[0].name_user;

        data.map(element => {
             tr = document.createElement("tr");
     
             td1 = document.createElement("td");
             td1.innerText = element.date_test;
             tr.appendChild(td1);
     
             td2 = document.createElement("td");
             td2.innerText = element.time_test;
             tr.appendChild(td2);
     
             elementTable.appendChild(tr);
               
      
        });
         
    }
     getDataSesionStorage();


});



